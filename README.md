# Towa GDPR Plugin

## Description
This Plugin is a cookie notice plugin conforming with the gdpr standards, implemented by the EU. 
- Backenduser can define different tracker with custom js code, description, name, and a link to the vendors data protection declarations.
- Frontendusers can customize what trackers they want and which they don't. These settings can be revoked anytime

## Requirements
- php ^7.2
- [composer](https://getcomposer.org/) ( for now, working on version without composer)
- [Advanced custom fields pro (>=v5.0)](advancedcustomfields.com)

## Setup 
You will find the steps for the installation in the `docs` folder.

## Usage
You will find the steps required for the usage in the `docs` folder.

## Contributers
- [Towa Digital Agency](https://www.towa.at)
